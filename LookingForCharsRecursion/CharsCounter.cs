﻿using System;

namespace LookingForCharsRecursion
{
    public static class CharsCounter
    {
        public static int GetCharsCount(string str, char[] chars)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str), "String is null.");
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars), "Array is null.");
            }

            int counter = 0;
            int currStringIndex = 0;
            int currCharIndex = 0;

            return NextStep(str, chars, counter, currStringIndex, currCharIndex);
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str), "String is null.");
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars), "Array is null.");
            }

            if (startIndex < 0 || startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is out of range.");
            }

            if (endIndex < 0 || endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex), "End index is out of range.");
            }

            if (startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index cannot be bigger than end index.");
            }

            int counter = 0;
            int currCharIndex = 0;

            return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex);
        }

        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex, int limit)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str), "String is null.");
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars), "Array is null.");
            }

            if (startIndex < 0 || startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is out of range.");
            }

            if (endIndex < 0 || endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(endIndex), "End index is out of range.");
            }

            if (limit < 0 || limit > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(limit), "Limit value is out of range.");
            }

            if (startIndex > endIndex)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index cannot be bigger than end index.");
            }

            int counter = 0;
            int currCharIndex = 0;

            return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex, limit);
        }

        public static int NextStep(string str, char[] chars, int counter, int currStringIndex, int currCharIndex)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str), "String is null.");
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars), "Array is null.");
            }

            if (counter < 0)
            {
                throw new ArgumentException("Initial counter value shouldn't be negative.", nameof(counter));
            }

            if (currStringIndex < 0 || currStringIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(currStringIndex), "Start index is out of range.");
            }

            if (currStringIndex < str.Length)
            {
                if (str[currStringIndex] == chars[currCharIndex])
                {
                    counter++;
                    currStringIndex++;
                    currCharIndex = 0;
                    return NextStep(str, chars, counter, currStringIndex, currCharIndex);
                }
                else if (str[currStringIndex] != chars[currCharIndex] && currCharIndex < chars.Length - 1)
                {
                    currCharIndex++;
                    return NextStep(str, chars, counter, currStringIndex, currCharIndex);
                }
                else
                {
                    currStringIndex++;
                    currCharIndex = 0;
                    return NextStep(str, chars, counter, currStringIndex, currCharIndex);
                }
            }
            else if (currStringIndex == str.Length)
            {
                return counter;
            }

            return NextStep(str, chars, counter, currStringIndex, currCharIndex);
        }

        public static int NextStep(string str, char[] chars, int counter, int startIndex, int endIndex, int currCharIndex)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str), "String is null.");
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars), "Array is null.");
            }

            if (counter < 0)
            {
                throw new ArgumentException("Initial counter value shouldn't be negative.", nameof(counter));
            }

            if (startIndex < 0 || startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is out of range.");
            }

            if (startIndex <= endIndex)
            {
                if (str[startIndex] == chars[currCharIndex])
                {
                    counter++;
                    startIndex++;
                    currCharIndex = 0;
                    return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex);
                }
                else if (str[startIndex] != chars[currCharIndex] && currCharIndex < chars.Length - 1)
                {
                    currCharIndex++;
                    return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex);
                }
                else
                {
                    startIndex++;
                    currCharIndex = 0;
                    return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex);
                }
            }
            else
            {
                return counter;
            }
        }

        public static int NextStep(string str, char[] chars, int counter, int startIndex, int endIndex, int currCharIndex, int limit)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str), "String is null.");
            }

            if (chars is null)
            {
                throw new ArgumentNullException(nameof(chars), "Array is null.");
            }

            if (counter < 0)
            {
                throw new ArgumentException("Initial counter value shouldn't be negative.", nameof(counter));
            }

            if (startIndex < 0 || startIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "Start index is out of range.");
            }

            if (limit < 0 || limit > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(limit), "Limit value is out of range.");
            }

            if (startIndex <= endIndex && counter < limit)
            {
                if (str[startIndex] == chars[currCharIndex])
                {
                    counter++;
                    startIndex++;
                    currCharIndex = 0;
                    return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex, limit);
                }
                else if (str[startIndex] != chars[currCharIndex] && currCharIndex < chars.Length - 1)
                {
                    currCharIndex++;
                    return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex, limit);
                }
                else
                {
                    startIndex++;
                    currCharIndex = 0;
                    return NextStep(str, chars, counter, startIndex, endIndex, currCharIndex, limit);
                }
            }
            else
            {
                return counter;
            }
        }
    }
}
